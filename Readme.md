# LED HTTP Server for Raspberry Pi 1 Model B

**Author:** Octavio E. Lobato-Buendia  
**Class:** CS3640 Intro to Networks and Their Applications  


**NOTE:** This project assumes you are using a Mac. If you are using a Windows PC you may need to do some digging
around yourself to run the program.


## Application of Program

This program allows you to run an HTTP server on a Raspberry Pi 1 model B. The server will listen for POST requests that update the brightness of one out of three LEDs. See [Execution Instructions](#markdown-header-execution-instructions) for instructions on how to run the program.


## Environment Reproduction

### Raspberry Pi Setup

Replicating the environment is pretty simple and straight forward.
You will need:

1. Raspberry Pi 1 Model B
2. 16GB SD card (I used 32GB)
3. Built-in or USB adaptor to read/write onto an SD card.
4. An accessible router with one spare ethernet socket.

Follow, ***[Headless setup: no keyboard, display or frustration](https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=74176)***,
a very nice guide written by fearless to setup your Raspberry Pi from scratch.


**NOTE:** You may skip step 6, "Accessing the window server" since you will not need to use a GUI.

### Dependency Setup

After you have your Raspberry Pi set up and ready to go, log in using
[SSH](https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=74176) (step 3) and install Java 8 by typing in the
following command in the terminal:
~~~
sudo apt-get update && sudo apt-get install oracle-java8-jdk
~~~

The next thing you have to do is obtain Git on your Raspberry Pi. To do this simply type the following command in your
terminal:
~~~
sudo apt-get install git
~~~

Now shutdown and unplug your Raspberry Pi:
~~~
sudo shutdown -h now
~~~

After shutting down your Raspberry Pi, it's time to set up the hardware. If you're not experienced in hardware don't panic, I wasn't either, its fairly simple and straight forward.

### Hardware Setup
##### Breadboard Setup

**NOTE:** While working on the hardware, always be sure the Raspberry Pi is powered **off** and do **not** touch any of
the hardware directly, this could cause the static you have built up to jump over to the hardware and fry the Raspberry
Pi.

First lets obtain the resources required:

* 1 Breadboard
* 4 prototyping wires, preferably with wire connectors (I had to buy my own
  wire connectors, which limited me to certain GPIO's I could use)
* 3 LEDs
* 3 220Ω Resistors

Follow the provided steps below:

1. Place LEDs
    * Position the breadboard in front of you so that the plus (+), minus (-), and A to J symbols are on 
      the top.
    * On the left side of the breadboard place the red LED's long hand into column A,  
      row 34. Then place the short hand of the red LED into the negative (-) column, row 34.
    * Apply the same steps above for the yellow LED, row 37, then the green LED, row 40. Be sure to leave     
      two
      empty rows in between each LED.
2. Place resistors
    * With the LEDs set, place a resistor on each row of each LED. Place the left hand  
      of the resistors on column E and the right hand of the resistors on column F.
3. Place prototyping wires
    * With the LEDs set and the resistors set, place a prototyping wire on each row of  
      each LED/resistor, column J.
    * Moving over by the LEDs, place another wire on the negative (-) column, row 30.  
      Make sure there are two open rows in between this wire and the red LED.

Your breadboard should now look like this:

* [Breadboard example 1](assets/breadboard1.jpg)  
* [Breadboard example 2](assets/breadboard2.jpg)

##### Pin Setup

**NOTE:** To connect my wires to the pins, I used telephone butt connectors, + 26-22 gauge. I do not recommend this
because the butt connectors are too wide. This made me use extra connectors to prevent them from wobbling and it
limited me to specific GPIOs, I could not place three butt connectors in a row. It was the best I could find at our local electronics store. These connectors are yellow.
If you do not want to use connectors, you can wrap the wires around the pins, this is however a delicate/tedious
process.

Connecting the breadboard to the Raspberry Pi:

1. Open up your Raspberry Pi
    * You will be using pin numbers: 6, 11, 13, and 16. See [Pin Layout](http://pi4j.com/images/p1header.png)
2. Connect the wire closets to the red LED to pin 6
3. Connect the wire linking to the red LED to pin 11
4. Connect the wire linking to the yellow LED to pin 13
5. Connect the wire linking to the green LED to pin 16

If you used butt connectors, your Raspberry Pi should now look like this:

* [Raspberry Pi example 1](assets/raspberryPi1.jpg)

Reconnect your ethernet cable and the power source to the Raspberry Pi then read the
[Execution Instructions](#markdown-header-execution-instructions) below. You will then be ready to run the program.

## Execution Instructions

Assuming you have followed the [Environment Reproduction](#markdown-header-environment-reproduction) instructions
described above, you are now ready to clone this repository. SSH to your Raspberry Pi and follow the instructions below:

Navigate to which ever directory you want the program to be saved to. Then Clone this repository onto your Raspberry Pi by typing:
~~~
git clone https://TD-Y@bitbucket.org/TD-Y/led_httpserver.git
~~~

Navigate into the ***led_httpserver*** directory:
~~~
cd led_httpserver
~~~

Run the startup script and follow the prompt:
~~~
./startup.sh
~~~

**NOTE:** Since this is the first time you will be running the program, I suggest you select option 3. After compiling at least once, you may select option 1.

The HTTP server should now be running on your Raspberry Pi! To create a POST request we will be using [curl](https://curl.haxx.se). All versions
of OS X starting with Jaguar (Released August 24th, 2002) should already have curl installed. If you haven't updated
your OS X since then, I believe you have a bigger issue on hand :).

Using curl is very simple. Open a new terminal window on your Mac and type the example curl command:
~~~
curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"index":11, "brightness":100}' http://Raspberry.Pi.IP.Address:4567/led
~~~

**NOTE:** Be sure to replace "Raspberry.Pi.IP.Address" with the actual IP Address of your Raspberry Pi.

After running the command shown above, you should see the red LED light up! If you want to light up a different LED or
change the brightness, change the number values in the following text of the curl command above:
~~~
{"index":11, "brightness":100}
~~~

To exit the program, in your Raspberry Pi terminal, press Ctrl-C.