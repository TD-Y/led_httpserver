#!/bin/bash

# Startup script to compile/run led_httpserver
PS3='Select a number to continue: '
options=("Run" "Compile" "Compile and Run" "Exit")
select opt in "${options[@]}"
do
    case $opt in
        "Run")
            echo "Running..."
            sudo java -cp ./lib/'*':./src Main
            break
            ;;
        "Compile")
            echo "Compiling..."
            javac -cp ./lib/'*' ./src/enums/*.java ./src/payload/interfaces/*.java ./src/payload/*.java ./src/Main.java
            echo "Compilation complete!"
            ;;
        "Compile and Run")
            echo "Compiling..."
            javac -cp ./lib/'*' ./src/enums/*.java ./src/payload/interfaces/*.java ./src/payload/*.java ./src/Main.java
            echo "Running..."
            sudo java -cp ./lib/'*':./src Main
            break
            ;;
        "Exit")
            echo "Bye!"
            break
            ;;
        *)
            echo "No such option, try again"
            ;;
    esac
done