/**
 * Change directories to where ever Main.java is located.
 * To compile: javac -cp ~/led_httpserver/lib/'*' enums/HTTPStatusCode.java enums/LEDIndex.java payload/PostPayload.java payload/RequestPayload.java Main.java
 * To run: java -cp ~/led_httpserver/lib/'*':../src Main
 *
 * Once running, connect by opening up a browser and going to the specified URL.
 * URL: http://192.168.1.136:4567/led
 * Generic URL: http://Raspberry.Pi.IP.Address:4567/led
 *
 * To run a curl post request in terminal:
 * curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"index": 0, "brightness": 100}' http://localhost:4567/led
 */

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.pi4j.io.gpio.*;
import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.SoftPwm;
import enums.HTTPStatusCode;
import enums.LEDIndex;
import payload.PostPayload;

import static spark.Spark.*;

public class Main {

    private static final GpioController GPIO = GpioFactory.getInstance();

    public static void main(String[] args) throws InterruptedException {

        //Initialize non PMw pins for PMW
        Gpio.wiringPiSetup();
        for(LEDIndex led : LEDIndex.values()){
            SoftPwm.softPwmCreate(led.gpio().getAddress(), 0, 100);
        }

        // Post request by http://Raspberry.Pi.IP.Adress:4567/led
        post("/led", ((request, response) -> {
            try {
                // Map request object.
                ObjectMapper mapper = new ObjectMapper();
                PostPayload postPayload = mapper.readValue(request.body(), PostPayload.class);
                // Check for errors.
                String message = postPayload.validate();
                if(!message.isEmpty()){
                    response.status(HTTPStatusCode.CONFLICT.code());
                    message += "\n";
                    return message;
                }
                // No errors found, update pin brightness.
                SoftPwm.softPwmWrite(LEDIndex.findEnumFromIndex(postPayload.getIndex()).gpio().getAddress(), postPayload.getBrightness());
                response.status(HTTPStatusCode.OK.code());
                return "";
            } catch (UnrecognizedPropertyException e) {
                response.status(HTTPStatusCode.CONFLICT.code());

                StringBuilder message = new StringBuilder(e.getLocalizedMessage());
                message.append("\n");
                return message.toString();
            } catch (JsonMappingException e){
                response.status(HTTPStatusCode.CONFLICT.code());

                StringBuilder message = new StringBuilder(e.getLocalizedMessage());
                message.append("\n");
                return message.toString();
            } catch (JsonParseException e){
                response.status(HTTPStatusCode.CONFLICT.code());

                StringBuilder message = new StringBuilder(e.getLocalizedMessage());
                message.append("\n");
                return message.toString();
            } catch (Exception e){
                response.status(HTTPStatusCode.INTERNAL_SERVER_ERROR.code());

                StringBuilder message = new StringBuilder(e.getLocalizedMessage());
                message.append("\n");
                return message.toString();
            }
        }));

        // Turns off all LEDs and shuts down GPIO when exiting program.
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for(LEDIndex led : LEDIndex.values()){
                SoftPwm.softPwmWrite(led.gpio().getAddress(), 0);
            }
            GPIO.shutdown();
        }));

    }
}

