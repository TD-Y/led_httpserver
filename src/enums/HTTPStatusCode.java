package enums;

/**
 * Author : Octavio E. Lobato-Buendia
 * Date : 4/15/16.
 *
 * HTTP status codes.
 */
public enum HTTPStatusCode {
    // Success
    OK(200),
    CREATED(201),
    NO_CONTENT(204),
    // Client Error
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    CONFLICT(409),
    INTERNAL_SERVER_ERROR(500);


    private final int code;

    HTTPStatusCode(int code){
        this.code = code;
    }

    public int code(){
        return code;
    }

}
