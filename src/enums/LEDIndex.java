package enums;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;

/**
 * Author : Efe
 * Date : 4/14/16.
 *
 * LED indexes corresponding to their Raspberry Pi GPIO pin number. The
 * {@link #index} corresponds to the Raspberry Pi P1 header.
 *
 * To figure out which pin number is which, position the Raspberry Pi so that
 * the pins are located the right side of the Pi facing you. Use the diagram below to
 * find the pin Index.
 *
 * The pins(*) and their corresponding index:
 *
 *  1 - * * - 2
 *  3 - * * - 4
 *  5 - * * - 6
 *  7 - * * - 8
 *  9 - * * - 10
 * 11 - * * - 12
 * 13 - * * - 14
 * 15 - * * - 16
 * 17 - * * - 18
 * 19 - * * - 20
 * 21 - * * - 22
 * 23 - * * - 24
 * 25 - * * - 26
 *
 * This build only supports pin indexes 11, 13, and 16.
 *
 */
public enum LEDIndex {
    RED(11, RaspiPin.GPIO_00),
    YELLOW(13, RaspiPin.GPIO_02),
    GREEN(16, RaspiPin.GPIO_04);


    private final int index;
    private final Pin gpio;

    LEDIndex(int index, Pin gpio) {
        this.index = index;
        this.gpio = gpio;
    }

    public int index(){
        return index;
    }

    public Pin gpio(){
        return gpio;
    }

    /**
     * Finds the enum from the specefied index.
     * @param index to find enum from.
     * @return null if not found, enum if found.
     */
    public static LEDIndex findEnumFromIndex(int index){
        for(LEDIndex ledIndex : LEDIndex.values()){
            if(index == ledIndex.index()) return ledIndex;
        }
        return null;
    }

    /**
     * Creates a message of valid indexes.
     * @return valid indexes.
     */
    public static String getValidIndexes(){
        StringBuilder validIndexes = new StringBuilder("Valid indexes include");
        for(LEDIndex ledIndex : LEDIndex.values()){
            validIndexes.append(", ").append(ledIndex.index());
        }
        validIndexes.append(".");
        return validIndexes.toString();
    }
}
