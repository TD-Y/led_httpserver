package payload.interfaces;

/**
 * Author : Octavio E. Lobato-Buendia
 * Date : 4/14/16.
 *
 * All types of request payload classes should
 * implement this interface.
 */
public interface RequestPayload {

    /**
     * Validates JSON request to ensure
     * all fields are valid.
     */
    String validate();

}
