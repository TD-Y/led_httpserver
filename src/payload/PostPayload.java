package payload;

import enums.LEDIndex;
import payload.interfaces.RequestPayload;

/**
 * Author : Octavio E. Lobato-Buendia
 * Date : 4/14/16.
 *
 * Object to handle post request payloads.
 */
public class PostPayload implements RequestPayload {

    private int index;
    private int brightness;

    /**
     * Set default values if the post request
     * does not contain an index or brightness
     * property in the request body.
     */
    private PostPayload(){
        index = -1;
        brightness = -1;
    }

    public int getIndex(){
        return index;
    }

    public void setIndex(int index){
        this.index = index;
    }

    public int getBrightness(){
        return brightness;
    }

    public void setBrightness(int brightness){
        this.brightness = brightness;
    }

    @Override
    public String validate(){
        //todo - Improve by adding an error class.
        StringBuilder message = new StringBuilder();
        message.append(this.validateIndex())
               .append(this.validateBrightness());
        return message.toString();
    }

    /**
     * Validates {@link #index} to ensure
     * the value is a valid {@link LEDIndex#index()}
     * value.
     * @return empty string if valid, otherwise string with errors.
     */
    private String validateIndex(){
        if(LEDIndex.findEnumFromIndex(index) == null){
            return new StringBuilder("Index, ")
                    .append(index).append(", is not a valid index. ")
                    .append(LEDIndex.getValidIndexes()).append("\n").toString();
        }
        return "";
    }

    /**
     * Validates {@link #brightness} to ensure
     * the value within the range of 0 to 100.
     * @return empty string if valid, otherwise string with errors.
     */
    private String validateBrightness(){
        if(brightness < 0 || brightness > 100) {
            StringBuilder message = new StringBuilder("Brighness, ");
            message.append(brightness).append(", is not valid. Brightness must be within the range of 0 to 100\n");
            return message.toString();
        }
        return "";
    }
}
